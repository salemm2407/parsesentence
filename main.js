/**
 * @function parseSentence parse a word return firstleter  one random number inside length and last character leter
 * @param {*} string
 */
function parseSentence(string) {
  let splittingSentence = [];
  //validation for no sentence
  if (string.length <= 0) return "No sentence";
  if (/\s/.test(string)) {
    splittingSentence = string.split(" ");
  }
  if (/\S/.test(string)) {
    splittingSentence = string.split("-");
  }
  let outString = splittingSentence.map(separator);
  console.log(outString);
}

const separator = (word) => {
  let firstLetter;
  let randomIdx = Math.floor(Math.random() * word.length).toString();
  let lastLetter;
  [...word].forEach((letter, idx) => {
    idx === 0
      ? (firstLetter = word[0])
      : idx === word.length - 1
      ? (lastLetter = word[word.length - 1])
      : null;
  });
  let composed = `${firstLetter}${randomIdx}${lastLetter}`;

  console.log(firstLetter, lastLetter, randomIdx, composed, "🔥");
  return composed;
};

parseSentence("Smooth-el-patito-tenia-comezon");
